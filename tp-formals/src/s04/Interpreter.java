package s04;

import java.util.HashMap;

public class Interpreter {
  private static Lexer lexer;
  // TODO - A COMPLETER (ex. 3) 
  
  public static int evaluate(String e) throws ExprException {
    lexer = new Lexer(e);
    int res = parseExpr();
    // test if nothing follows the expression...
    if (lexer.crtSymbol().length() > 0)
      throw new ExprException("bad suffix");
    return res;
  }

  private static int parseExpr() throws ExprException {
    int res = 0;
    res = parseTerm();
    while(true){
      if(lexer.isPlus()){
        lexer.goToNextSymbol();
        res += parseTerm();
      }else if(lexer.isMinus()){
        lexer.goToNextSymbol();
        res -= parseTerm();
      } else{
        break;
      }
    }
    return res;
  }

  private static int parseTerm() throws ExprException {
    int res = 0;
    res = parseFact();
    while(true){
      if(lexer.isSlash()){
        lexer.goToNextSymbol();
        res /= parseFact();
      }else if(lexer.isStar()){
        lexer.goToNextSymbol();
        res *= parseFact();
      } else{
        break;
      }
    }
    return res;
  }

  private static int parseFact() throws ExprException {
     int res = 0;
    // TODO - A COMPLETER
    return res;
  }

  private static int applyFct(String fctName, int arg) throws ExprException {
    int res = 0;
    if(fctName.equals("abs")){
      res = Math.abs(arg);
    }else if(fctName.equals("sqr")){
      res =(int) Math.sqrt(arg);
    }else if(fctName.equals("cube")){
      res = (int) Math.pow(arg, 3);
    }else if(fctName.equals("sqrt")){
      res =(int) Math.sqrt(arg);
    }
    return res;
  }
}
