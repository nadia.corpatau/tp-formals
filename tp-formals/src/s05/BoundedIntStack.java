package s05;

import java.util.Random;

/* The capacity can't be negative. It's 
   forbidden to remove when empty, or to add
   when full. The collection cannot be empty
   (resp. full) after adding (resp. removing) 
*/
public class BoundedIntStack {
  private int[] buf;     
  private int   top = -1;

  /*@
   requires !isFull
   ensure buf.length== capacity
   @*/
  public BoundedIntStack(int capacity) {
    assert !isFull();
    buf = new int[capacity];
    assert buf.length== capacity;
  } 
  /*@
   requires top>0;
   ensure top >=0;
   @*/
  public int pop() {
    assert top>0;
    top--;
    assert top >= 0;
    return buf[top];

  }
    /*@
   requires x <= Integer.MAX_VALUE;
   ensure top < buf.length;
   @*/
  public void push(int x) {
    assert x <= Integer.MAX_VALUE;
    buf[++top] = x;
    assert top < buf.length;
  }
  public boolean isEmpty() {
    return top == -1; 
  }
  public boolean isFull() {
    return top >= buf.length-1;
  }
  
  //------------------------------------------------------------------------
  
  public static boolean areAssertionsEnabled() {
    int ec=0; 
    assert (ec=1) == 1;
    return ec == 1; 
  }
  
  static Random rnd = new Random();
  
  static void playWithBoundedStack(BoundedIntStack s) {
    int nOperations = 1000;
    for(int i=0; i<nOperations; i++) {
      boolean doPush = rnd.nextBoolean();
      boolean doPop = !doPush;
      if(doPush && !s.isFull())
        s.push(rnd.nextInt());
      if(doPop && !s.isEmpty())
        s.pop();
    }
  }
  
  public static void main(String[] args) {
    if(!areAssertionsEnabled()) {
      System.out.println("Please enable assertions, with '-ea' VM option !!");
      System.exit(-1);
    }
    int nStacks = 1000;
    int maxSize = 50;
    for(int i=0; i<nStacks; i++) {
      BoundedIntStack s = new BoundedIntStack(rnd.nextInt(maxSize));
      playWithBoundedStack(s);
    }
    System.out.println("End of demo!");
  }

}