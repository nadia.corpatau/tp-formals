package s05;

public class Ex1_2 {

    public static void main(String[] args) {
            int e = 6, f = 11;
            int c = Math.abs(e - f) % 2;
            // |e-f| % 2 == c
            if (e > 0) {
                e = e - 1;//  |(e+1)-f| % 2 == c
                f = f + 1;//  |(e+1)-(f-1)| % 2 == c
            } else {
                e = e + 1;//  |(e-1)-f| % 2 == c
                f = f - 1;//  |(e-1)-(f+1)| % 2 == c
                //  =|e-f|%2 + |-2|%2
            }              // =|e-f| % 2 == c
    }



    static int power(int b, int p) { // PRE: p > 0
        int res = 1, n = p;
        int x = b;              // x^n * res == bp
        while (n > 0) {         // x^n * res == bp && n>0
            if (n % 2 == 0) {   // x^n * res == bp && n>0 && n%2 == 0
                x = x * x;      // 1^n * res == bp && n>0 && n%2 == 0
                n = n / 2;      // 1^2n * res == bp && n>0 && n%2 == 0
                //----------------------------------------------------
            } else {            // x^n * res == bp && n>0 && n%2 != 0
                res = res * x;  // x^n * res/x == bp && n>0 && n%2 != 0
                n--;            //x^(n+1) * res/x == bp && n>0 && n%2 != 0
            }
        }
        return res;
    }
}


